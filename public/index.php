<?php
//permet de recuperer l'uri , l'url sans le nom de domaine
$uri = $_SERVER['REQUEST_URI'];
// à chaque fois qu'il y'a une condition if($result = match($uri, "/article/create") ) alors il s'agit d'une ruote qui appellera le controleur concerné 
//root par defaut 
//si l'uri correspond à */* alors on affiche la page qui correspond à la liste des articles du blog 
// redirige l'utilisateur vers l'url http://monsite.fr/articles
if($result = match($uri, "/")){
    header("location:../articles");
    die;
}

####################
### Crud Article ###
####################
//Route qui permet d'ajouter un article
if($result = match($uri, "/article/create")){
    require("../Controller/article/createArticle.php");
    die;
}
//Route qui permet de supprimer un article dont l'id a été pris en compte

if($result = match($uri, "/article/delete/:id")){
    require("../Controller/article/deleteArticle.php");
    die;
}
//Route qui permet d'afficher un article

if($result = match($uri, "/article/:id")){
    require("../Controller/article/displayArticle.php");
    die;
}
//Route qui permet d'afficher les articles 

if($result = match($uri, "/articles")){
    require("../Controller/article/displayArticles.php");
    die;
}
//Route qui permet de mettre à jour les articles dont l'id a été pris en compte


if($result = match($uri, "/article/update/:id")){
    require("../Controller/article/updateArticle.php");
    die;
}

#################
### Crud User ###
#################
if($result = match($uri, "/user/create")){
    require("../Controller/user/createUser.php");
    die;
}

if($result = match($uri, "/user/delete/:id")){
    require("../Controller/user/deleteUser.php");
    die;
}

if($result = match($uri, "/user/:id")){
    require("../Controller/user/displayUser.php");
    die;
}

if($result = match($uri, "/users")){
    require("../Controller/user/displayUsers.php");
    die;
}

if($result = match($uri, "/user/update/:id")){
    require("../Controller/user/updateUser.php");
    die;
}
// si l'utilisateur fait appel à une page qui n'existe pas alors le message "no routes are matching with" sera affiché suivi de l'uri renseigné
echo "No routes are matching with " . $uri;



function match($url, $route){
    $path = preg_replace('#:([\w]+)#', '([^/]+)', $route);
    $regex = "#^$path$#i";
    if(!preg_match($regex, $url, $matches)){
        return false;
    }
    return true;
}